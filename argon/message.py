from typing import List, Tuple, Any
from argon.type import Type


class Message:
    def __init__(self, content: List[Tuple[Any, Type]], sent_by, group,
                 timestamp, adapter: dict, message_id, flags=None):
        if flags is None:
            flags = []
        self.content = content
        self.sent_by = sent_by
        self.group = group
        self.timestamp = timestamp
        self.adapter = adapter
        self.message_id = message_id
        self.flags = flags
