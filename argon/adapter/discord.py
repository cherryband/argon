import re
import threading
import asyncio

from argon.adapter import Adapter
from argon.message import Message
from argon.type import MessageType, EventType
from argon.matcher import TypeMatcher


class DiscordAdapter(Adapter):
    def __init__(self, token:str, owner, identifier="!", require_mention=False):
        super(DiscordAdapter, self).__init__(identifier=identifier, require_mention=require_mention)
        self.capabilities = {MessageType.PLAINTEXT, MessageType.MARKDOWN,
                             MessageType.BINARY, MessageType.AUDIO,
                             MessageType.IMAGE, MessageType.VIDEO}

        from discord import Client
        self.client = Client()
        self.token = token
        self.owner = int(owner)

        self.bot_id = None
        self.callback = None
        self._id = None

        self.text_matcher = TypeMatcher([MessageType.PLAINTEXT])

        threading.Thread.__init__(self)

    def start(self):
        @self.client.event
        async def on_ready():
            self.bot_id = self.client.user.mention
            owner = self.client.get_user(self.owner)
            self._on_initialized(owner)

        @self.client.event
        async def on_message(message):
            if message.author == self.client.user:
                return
            self.eval(message)

        asyncio.run(self.client.run(self.token))

    def register_callback(self, func, _id):
        self.callback = func
        self._id = _id

    def eval(self, update):
        mentioned = "@" + self.bot_id in update.content

        if self.require_mention and not mentioned:
            return
        elif re.search(r"^/\w+@\w+", update.content) and not mentioned:
            return

        from_user = update.author.id
        from_group = update.channel
        message_id = update.id
        when = update.created_at

        flags = []
        if mentioned:
            flags += EventType.ARGON_MENTIONED

        content = [(update.content, MessageType.PLAINTEXT)]
        for attachment in update.attachments:
            content.append((attachment.url, MessageType.BINARY))

        message = Message(content,
                          {"user_id": from_user, "is_mod": from_user == self.owner},
                          from_group,
                          when,
                          {"_id": self._id, "ident": self.identifier, "adapter_type": self.__class__.__name__},
                          message_id,
                          flags=flags
                          )

        self.callback(message)

    def send_media(self, msgtype: MessageType, location: str, group = None, title = "", reply_to = None):
        is_reply = True if reply_to else False
        to = reply_to if reply_to else group

        if not to:
            raise ValueError ("send_media should have either group or reply_to variable.")

        if is_reply:
            send_func = self.reply
        else:
            send_func = self.send

        try:
            _file = open(location, "rb")

            send_func(title, to, _file = _file)
        except FileNotFoundError: ## Assumes web url, send as is
            send_func(location, to)

            send_to = reply_to.group if reply_to else group
            self.send(title, send_to)

    def send(self, message, group, _file=None):
        print(group)
        message = message.replace("_", "\\_")
        send_async = asyncio.create_task(group.send(message, file=_file))

    def reply(self, message, reply_to, _file=None):
        quote, _ = self.text_matcher.match(reply_to, None)
        to = reply_to.sent_by["user_id"]
        group = reply_to.group
        self.send("> {} \n<@{}> {}".format(quote, to, message), group, _file)
