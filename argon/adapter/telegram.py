import threading
import re
from argon.adapter import Adapter
from argon.message import Message
from argon.type import MessageType, EventType
from telegram import ChatMember
from telegram.ext import MessageHandler, Filters, Updater


class TelegramAdapter(Adapter):
    def __init__(self, token, owner, owner_id, identifier="[!/]"):
        super(TelegramAdapter, self).__init__(identifier=identifier)
        self.capabilities = {MessageType.PLAINTEXT, MessageType.HTML,
                             MessageType.BINARY, MessageType.AUDIO,
                             MessageType.IMAGE, MessageType.VIDEO}


        self.updater = Updater(token=token)
        self.dispatcher = self.updater.dispatcher

        self.bot = self.updater.bot
        self.bot_id = self.bot.get_me()["username"]

        self.owner = owner
        self.owner_id = owner_id

        threading.Thread.__init__(self)

    def run(self):
        handler = MessageHandler(Filters.text | Filters.command, self.eval)
        self.dispatcher.add_handler(handler)
        self.updater.start_polling()
        self._on_initialized(self.owner_id)

    def register_callback(self, func, _id):
        self.callback = func
        self._id = _id

    def eval(self, update, context):
        message = update.message.text.strip().replace("@" + self.bot_id, "")
        if re.search(r"^/\w+@\w+", message): return #If the message starts with a mention of other user, skip the message
        if "^" in message: #Special rule for Telegram Adapter: if the message is a reply, ^ is replaced with the original message it is replying to.
            try:
                message = message.replace("^", update.message.reply_to_message.text.strip())
            except AttributeError:
                pass

        from_user = update.message.from_user.name
        from_group = update.message.chat_id
        message_id = update.message.message_id
        when = update.message.date
        user_stat = context.bot.get_chat_member(from_group, update.message.from_user.id).status
        is_mod = user_stat == ChatMember.ADMINISTRATOR or user_stat == ChatMember.CREATOR

        flags = []
        if "@" + self.bot_id in message:
            flags += EventType.ARGON_MENTIONED

        self.callback(Message([(message, MessageType.PLAINTEXT)],
                              {"user_id": from_user, "is_mod": from_user == self.owner or is_mod},
                              from_group,
                              when,
                              {"_id": self._id, "ident": self.identifier, "adapter_type": self.__class__.__name__},
                              message_id,
                              flags=flags
                              )
                      )

    def send_media(self, msgtype: MessageType, location: str, group = None, title = "", reply_to = None):
        to = reply_to.group if reply_to else group
        if not to:
            raise ValueError ("send_media should have either group or reply_to variable.")

        reply_id = reply_to.message_id if reply_to else None

        send_with = self.bot.send_document

        # Telegram has filetype limitation on some types;
        # video should be .mp4 format
        # audio should be .mp3 format
        # other audio/video files will be sent as a generic file.
        if msgtype == MessageType.IMAGE:
            send_with = self.bot.send_photo
        elif msgtype == MessageType.VIDEO and location.endswith(".mp4"):
            send_with = self.bot.send_video
        elif msgtype == MessageType.AUDIO and location.endswith(".mp3"):
            send_with = self.bot.send_audio
        try:
            send_with(to, location, caption=title, reply_to_message_id=reply_id)
        except Exception as e:
            print(e)

    def send(self, message, group):
        self.bot.send_message(chat_id=group, text=message)

    def reply(self, message, reply_to):
        self.bot.send_message(chat_id=reply_to.group, text=message,
                              reply_to_message_id=reply_to.message_id)
