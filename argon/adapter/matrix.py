from datetime import datetime
import logging
import re
import traceback
import threading

from argon.adapter import Adapter
from argon.message import Message
from argon.constants import SOFTWARE_NAME
from argon.type import MessageType, EventType


class MatrixAdapter(Adapter):
    def __init__(self, server, username, password, owner, device_id=None, rooms=None, identifier="!", require_mention=False):
        super(MatrixAdapter, self).__init__(identifier=identifier, require_mention=require_mention)
        self.capabilities = {MessageType.PLAINTEXT, MessageType.MARKDOWN, MessageType.HTML,
                             MessageType.BINARY, MessageType.AUDIO,
                             MessageType.IMAGE, MessageType.VIDEO}

        self.server = server
        self.username = username
        self.password = password
        self.owner = owner
        self.rooms = rooms if bool(rooms) else []
        self.device_id = device_id
        self.handled_rooms = {}

        self.logger = logging.getLogger("MatrixAdapter")
        threading.Thread.__init__(self)
        logging.basicConfig(level=logging.INFO)

    def find_element_in_list(self, element, list_element):
        try:
            index_element = list_element.index(element)
            return index_element
        except ValueError:
            return None

    def display_name_for_room(self, room, user_id):
        for member in room._members:
            if member.user_id == user_id:
                return member.get_display_name()
        else:
            return None

    def register_callback(self, func, _id):
        self.execute = func
        self._id = _id

    def send(self, message, room):
        # todo: markdownify?
        self.logger.info("To room ``{}'': ``{}''".format(room, message))
        room.send_text(message)

    def reply(self, message, reply_to):
        to = reply_to.sent_by["user_id"]
        room = reply_to.group

        # todo: handle apiaceae and pals
        formatted_message = '<a href="https://matrix.to/#/{}">{}</a>: {}'.format(
            to, self.display_name_for_room(room, to), message)
        plain_text_message = '{}: {}'.format(
            self.display_name_for_room(room, to), message)
        self.logger.info("To ``{}'' in room ``{}'': ``{}''".format(
            to, room. room_id, formatted_message))
        room.send_html(formatted_message, body=plain_text_message)

    def eval(self, room, event):
        # Ignore our own events
        if event['sender'] == self.client.user_id:
            return

        mentioned = self.display_name_for_room(room.room_id, self.client.user_id) in event['content']['body']
        # Force only mentions
        if self.require_mention and not mentioned:
            return

        user = event['sender']
        message = event['content']['body']
        origin_server_ts = datetime.fromtimestamp(
            int(event['origin_server_ts'])/1000)

        # Check if the message was sent through a bridging bot

        # First we check the sender's nick
        # Remember that event['sender'] is the Matrix id,
        # @<network>_<name>:<domain>
        m_user = re.fullmatch(
            r'@(?:.+_)?(apiaceae|xxxx_|^^^^):.+', event['sender'])
        if m_user:
            # Then we check the message.
            # Matrix escapes IRC color codes to plain text
            m_message = re.fullmatch(
                r'<(?:\x03[^\x02]*\x02*)?([^>\x03]+)\x03?> *: (.+)', event['content']['body'])

            if m_message:
                # Move information so that we think this message came from that user
                # Append [proxy] to avoid impersonation
                proxy_user = m_user[1]
                user = "{}[{}]".format(m_message[1], proxy_user)
                message = m_message[2]

        self.logger.info("Received message in ``{}'' from ``{}'': ``{}''".format(
            room.display_name, user, message))

        # Since Matrix has no concept of private rooms, make them optional
        # and force mention check
        ident = self.identifier_optional

        flags = []
        if mentioned:
            flags += EventType.ARGON_MENTIONED

        # Bot reply signature: message, to, room, adapter ID
        self.execute(Message([(message, MessageType.PLAINTEXT)],
                             {"user_id": user, "is_mod": user == self.owner},
                             room,
                             origin_server_ts,
                             {"_id": self._id, "ident": self.identifier, "adapter_type": self.__class__.__name__},
                             user,
                             flags
                             ))

    def handle_invite(self, room_id, state):
        self.logger.info("Got invite to room: {}, joining".format(room_id))
        room = self.client.join_room(room_id)
        self.logger.info(
            "Listening to events from {}".format(room.display_name))
        room.add_listener(self.eval, event_type="m.room.message")
        self.handled_rooms[room_id] = room

    def run(self):
        from matrix_client.client import MatrixClient
        from matrix_client.api import MatrixRequestError

        self.client = MatrixClient(self.server)
        try:
            self.logger.info("Logging in...")
            response = self.client.login(self.username, self.password,
                              device_id=self.device_id)
            print (response)
        except MatrixRequestError as e:
            if e.code == 403:
                self.logger.error("Bad username/password")
            else:
                self.logger.error(traceback.format_exc())
            return
        except Exception:
            self.logger.error(traceback.format_exc())
            return

        self.logger.info("Logged in succesfully!")

        self.client.api.update_device_info(self.device_id, SOFTWARE_NAME)

        self.client.add_invite_listener(self.handle_invite)

        for room_id, room in self.client.get_rooms().items():
            self.logger.info(
                "Listening to events from {}".format(room.display_name))
            room.add_listener(self.eval, event_type="m.room.message")
            self.handled_rooms[room_id] = room

        for room_id in self.rooms:
            if self.handled_rooms[room_id]:
                continue
            room = self.client.join_room(room_id)
            self.logger.info(
                "Listening to events from {}".format(room.display_name))
            room.add_listener(self.eval, event_type="m.room.message")
            self.handled_rooms[room_id] = room

        self.client.start_listener_thread()

        self.client.sync_thread.join()

        self._on_initialized(self.owner)
