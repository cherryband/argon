SOFTWARE_NAME = "ArgonBot"
VERSION = "2a20200510"
DESCRIPTION = "A multi-protocol bot"
AUTHORS = "imsesaok, midgard and amyspark"
DESCRIPTION_AND_AUTHORS = "{} by {}".format(DESCRIPTION, AUTHORS)
