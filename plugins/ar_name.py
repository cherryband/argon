#!/usr/bin/env python3

import random
import os
from argon.command import Command
from argon.matcher import RegexMatcher


letter_stat = dict()
collocation_stat = dict()
LETTER_STAT_TOTAL = 0
COLLOCATION_STAT_TOTAL = 0

def import_stat(stat_dict, filename):
    '''
    helper function for importing stat file.
    '''
    stat_total = 0
    with open(filename) as stat_file:
        for line in stat_file:
            keyvalue = line.split(',')
            key = keyvalue[0]
            value = int(keyvalue[1])

            stat_dict[key] = value
            stat_total += value

    return stat_total

filepath = os.path.join(os.path.dirname(__file__), 'collocation_stat.txt')
COLLOCATION_STAT_TOTAL = import_stat(collocation_stat, filepath)


def weighted_random(stat_dict, stat_total):
    '''
    A weighted random selector.
    Works in conjunction with gen_subdict() to get the next character based on statistics.
    '''
    randint = random.randint(1, stat_total)

    selection = None
    for key, value in stat_dict.items():
        randint -= value
        if randint <= 0:
            selection = key
            break

    return selection

def gen_subdict(stat_dict, letter):
    '''
    A dict() generator that filters the stat dict()
    and provides the total stat value needed for weighted_random()
    '''
    subdict = {key:value for key, value in stat_dict.items() if key[0] is letter}
    stat_total = 0

    for key, value in subdict.items():
        stat_total += value
    return subdict, stat_total

def gen_word(min_len=2, max_len=100):
    '''
    A function that generates a random, readable word.
    '''
    generated = weighted_random(*gen_subdict(collocation_stat, ' '))

    while (generated[-1] != ' ' or len(generated.strip()) < min_len) and len(generated) < max_len:
        generated = generated.strip()
        next_letter = weighted_random(*gen_subdict(collocation_stat, generated[-1]))
        generated += next_letter[1]

    return generated.strip()

GEN_LOWER_LIMIT = 3
GEN_UPPER_LIMIT = 15
NAMES_UPPER_LIMIT = 40

def generate_word(match, message, bot):
    words = match.group("count")
    buffer = ""

    word_count = 1
    if words is not None:
        word_count = int(words)
        if word_count > NAMES_UPPER_LIMIT:
            buffer = "Number too large ({} > {}).".format(word_count, NAMES_UPPER_LIMIT)
            word_count = 0

    for i in range(word_count):
        buffer += gen_word(GEN_LOWER_LIMIT, GEN_UPPER_LIMIT)
        if i != word_count - 1:
            buffer += ", "

    bot.reply(buffer, message)


def register_with(argon):
    argon.add_commands(
        Command(RegexMatcher(r"!!name(?: (?P<count>\d{1,4}))?"),
                "name (<number of names>)",
                "Generate random, pronounceable names.",
                generate_word),
    )
