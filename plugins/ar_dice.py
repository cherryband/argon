#!/usr/bin/env python3

import dice
import re
import numbers
import pyparsing
from argon.command import Command
from argon.matcher import RegexMatcher


def format_six_faced_die(value):
    return ("⚀", "⚁", "⚂", "⚃", "⚄", "⚅")[value-1]


def format_fudge_die(value):
    return ("⊟", "⊠", "⊞")[value+1]


def format_other_die(value):
    return "[{}]".format(value)


def dice_fun(*args):
    spec = args[0].group("dicespec")
    message = args[1]
    bot = args[2]

    if not spec:
        spec = "1d6"

    if "," in spec:
        specs = spec.split(",")
        for x in specs:
            dice_fun(x, message, bot)
        return

    # Some cases we would like to handle that the dice library does not support
    if re.fullmatch(r'\d+', spec):
        spec = "1d" + spec
    elif re.fullmatch(r'\d+d', spec):
        spec = spec + "6"

    # Let the dice library parse the format
    try:
        dice_expression = dice.parse_expression(spec)[0]
    except pyparsing.ParseBaseException as e:
        # Feed to DiceBaseException to use the supplied pretty_print function
        e = dice.exceptions.DiceBaseException.from_other(e)
        bot.reply("Unsupported dice format:\n{}".format(e.pretty_print()), message)

        return

    try:
        # Roll the dice
        dice_result = dice_expression.evaluate_cached()
    except dice.exceptions.DiceBaseException as e:
        bot.reply("Unsupported dice format:\n{}".format(e.pretty_print()), message)

        return

    # Format the result
    try:
        # Format 6-faced dice differently
        if dice_expression.min_value == 1 and dice_expression.max_value == 6:
            formatter = format_six_faced_die
        elif dice_expression.min_value == -1 and dice_expression.max_value == 1:
            formatter = format_fudge_die
        else:
            formatter = format_other_die

        if dice_expression.amount == 1:
            format_string = "{graphic}"
        else:
            format_string = "{total}: {graphic}"

        graphic = ' '.join(map(formatter, dice_result))
        if len(graphic) > 400:
            graphic = "(individual result too long)"
        formatted = format_string.format(graphic= graphic,
                                         total=sum(dice_result))

    except AttributeError:
        # If the dice_expression doesn't have min_value, it's probably a complex spec that evaluates to just a number.
        # Check this explicitly to avoid leaking information.
        if isinstance(dice_result, numbers.Number):
            formatted = str(dice_result)
        else:
            formatted = "Unable to recognize dice result"
            print("Unable to recognize dice result: {}".format(dice_result))

    bot.reply(formatted, message)

    # dice_expression.sides is broken: it gives 1 for fudge dice (-1 through 1), who clearly have 3 sides
    try:
        if dice_expression.min_value == dice_expression.max_value:
            bot.send(message.adapter['id_'], "(seriously tho?)",
                     message.group)
    except AttributeError:
        pass


def register_with(argon):
    argon.add_commands(
        Command(RegexMatcher(r"!!dice(?: (?P<dicespec>.+))?"),
                "dice (<dice specification>)",
                "Roll a dice.", dice_fun),
    )
