import random
import jellyfish
from argon.command import Command
from argon.matcher import RegexMatcher

questions = {}

positives = ["It is certain.",
             "It is decidedly so.",
             "Without a doubt.",
             "Yes - definitely.",
             "You may rely on it.",
             "As I see it, yes.",
             "Most likely.",
             "Outlook good.",
             "Yes.",
             "Signs point to yes.",
            ]

non_committals = ["Reply hazy, try again.",
                  "Ask again later.",
                  "Better not tell you now.",
                  "Cannot predict now.",
                  "Concentrate and ask again.",
                 ]

negatives = ["Don't count on it.",
             "My reply is no.",
             "My sources say no.",
             "Outlook not so good.",
             "Very doubtful.",
            ]

def roll_answer(category):
    if category < 0:
        return random.choice(negatives)
    if category > 0:
        return random.choice(positives)

    return random.choice(non_committals)

def roll_category():
    posnum = len(positives)
    neutnum = len(non_committals)
    negnum = len(negatives)

    randnum = random.randint(1, posnum + neutnum + negnum)
    if randnum <= posnum:
        return 1
    randnum -= posnum

    if randnum <= neutnum:
        return 0
    randnum -= neutnum

    return -1

def roll():
    category = roll_category()
    return (category, roll_answer(category))

def find_answer(question):
    for q in questions:
        if jellyfish.jaro_distance(q, question) > 0.8:
            print (str(jellyfish.jaro_distance(q, question)))
            return (q, questions[q])
    return (None, None)

def eight_ball (match, message, bot):
    question = match.group("question")
    if question is None:
        bot.reply("Try again with a yes or no question.", message)
        return

    saved, cat = find_answer(question)

    if cat == 0:
        cat = roll_category()
        questions[saved] = cat
    elif not cat:
        cat = roll_category()
        questions[question] = cat


    reply = roll_answer(cat)
    bot.reply(reply, message)


def register_with(argon):
    argon.add_commands(
        # The Magical 8 Ball
        Command(RegexMatcher(r"(?P<ident>!!)8ball( (?P<question>.+))?"),
                "8ball <yes-or-no question>",
                "Consult the Magic 8 Ball for the answer to your question.", eight_ball),
    )
