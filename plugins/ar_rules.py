#!/usr/bin/env python3

import re, copy
from argon.command import Command
from argon.message import Message
from argon.type import MessageType, EventType
from argon.matcher import RegexMatcher, StringMatcher


def nested_eval(match, message, bot, command):
    message.flags.append(FLAG_NESTED)
    message.content = [(command, MessageType.PLAINTEXT)]
    bot.process(message)


def always_false(*args, **kwargs):
    return False


FLAG_REGEX_DISABLE = 4401
FLAG_NESTED = 4402
FLAG_RULE = 4403

DISABLED_NOTICE = "This feature is currently disabled."


def add(match, message, bot):
    if FLAG_REGEX_DISABLE in message.flags:
        bot.reply(DISABLED_NOTICE, message)
        return

    try:
        if not match.group('condition') or not match.group('command'):
            return

        condition = str(match.group('condition')).strip()
        command_str = str(match.group('command')).strip()
    except AttributeError:
        return


    try:
        re.compile(condition)
    except re.error as e:
        bot.reply("Rule not accepted: regular expression is not valid. ({})".format(e), message)
        return

    def on_exec(match, message, bot):
        nested_eval(match, message, bot, command_str)

    group = message.group
    adapter_id = message.adapter["_id"]

    def exec_condition(match, message, bot):
        return message.group == group \
               and message.adapter["_id"] == adapter_id \
               and FLAG_REGEX_DISABLE not in message.flags \
               and FLAG_NESTED not in message.flags

    conditional_cmd = Command(RegexMatcher(condition),
                              condition,
                              command_str,
                              on_exec, flags=[FLAG_RULE],
                              display_condition=always_false,
                              exec_condition=exec_condition)

    command_works = False

    condition_message = message
    condition_message.content = [(condition, MessageType.PLAINTEXT)]
    command_message = Message([(command_str, MessageType.PLAINTEXT)],
                              message.sent_by,
                              message.group,
                              message.timestamp,
                              message.adapter,
                              0,
                              flags=message.flags
                              )
    command_message.flags.append(FLAG_NESTED)

    for command in bot.commands:
        if command.exec_condition(match, message, bot):
            if condition == command.title and FLAG_RULE in command.flags:
                bot.reply("Rule not accepted: rule already exists. ('{}' -> {})\n"
                          "Remove existing command before adding a new one.".format(command.regex, command.description),
                          message)
                return
            # The condition matches the regex of the existing command.
            elif command.matcher.match(condition_message, bot) and not isinstance(command.matcher, StringMatcher):
                bot.reply("Rule not accepted: you cannot override existing commands.",
                          message)
                return

            # Test if the resulting command does exist
            elif command.test(command_message, bot):
                command_works = True

    if not command_works:
        bot.reply("rule not accepted: command '{}' is not available.".format(
            command_str), message)
        return

    bot.commands.append(conditional_cmd)
    bot.reply("Rule successfully created: '{}' -> {}".format(conditional_cmd.title,
                                                             conditional_cmd.description), message)


def remove(match, message, bot):
    try:
        condition = match.group("condition")
    except AttributeError:
        return

    for command in bot.commands:
        if FLAG_RULE in command.flags:
            if condition == command.title and command.exec_condition(match, message, bot):
                bot.commands.remove(command)
                bot.reply("Rule successfully deleted.", message)
                return

    bot.reply("Rule does not exist.", message)


def remove_matching(match, message, bot):
    try:
        condition = match.group("condition")
    except AttributeError:
        return

    del_count = 0
    for command in bot.commands:
        if FLAG_RULE in command.flags:
            message.content = [(condition, MessageType.PLAINTEXT)]
            if command.test(message, bot):
                bot.commands.remove(command)
                del_count += 1

    if del_count > 0:
        bot.reply("{} rule{} ha{} been successfully deleted.".format(del_count,
                                                                     "s" if del_count != 1 else "",
                                                                     "ve" if del_count != 1 else "s"),
                  message)
    else:
        bot.reply("No matching rules exist.", message)


def display_paginated(_list, index):
    linecount = 4
    list_length = len(_list)
    start_index = index * linecount
    end_index = start_index + linecount

    if index < 0:
        raise IndexError("Invalid number: use a number above or equal to 1.")

    maximum = int(-(-list_length // linecount))
    if list_length < end_index:
        end_index = list_length
        if end_index <= start_index:
            raise IndexError(
                "Invalid number: use a number below or equal to " + str(maximum) + ".")

    message = ""
    for i in range(start_index, end_index):
        command = _list[i]
        message += "'{}' -> {}\n".format(command.title, command.description)

    return maximum, message.strip()


def show(match, message, bot):
    if FLAG_REGEX_DISABLE in message.flags:
        bot.reply(DISABLED_NOTICE, message)
        return

    rule_list = tuple(x for x in bot.commands
                      if FLAG_RULE in x.flags and x.exec_condition(match, message, bot))

    args = match.groupdict()
    condition = ""
    if args["page"]:
        index = int(args['page'].strip()) - 1
    elif args["condition"]:
        condition = args['condition']
    else:
        index = 0

    if condition:
        for command in rule_list:
            if condition == command.title and FLAG_RULE in command.flags and command.exec_condition(match, message, bot):
                bot.reply("'{}' -> {}".format(command.title, command.description),
                          message)
                return
        bot.reply("Rule with condition '{}' not found.".format(condition), message)

    else:
        rule_count = len(rule_list)
        if rule_count > 0:
            try:
                maximum, list_message = display_paginated(rule_list, index)

                reply = "Rules: {current} out of {maximum}\n".format(current=index + 1, maximum=maximum)
                reply += "Total of {total} rule{s} {are} available.\n" \
                    .format(total=rule_count, s="s" if rule_count > 1 else "", are="are" if rule_count > 1 else "is")
                reply += list_message
                bot.reply(reply, message)
            except IndexError as e:
                bot.reply(str(e), message)
        else:
            bot.reply("No rules defined.", message)


def set_regexif(match, message, bot):
    if str(match.group("bool")) in "false":
        bot.flags.append(FLAG_REGEX_DISABLE)
    elif str(match.group("bool")) in "true" and FLAG_REGEX_DISABLE in bot.flags:
        bot.flags.remove(FLAG_REGEX_DISABLE)
    bot.reply("regexif = " + str(FLAG_REGEX_DISABLE not in bot.flags), message)


def register_with(argon):
    argon.add_commands(
        # Add rule
        Command(RegexMatcher(r"(?P<ident>!!)if(?: (?P<condition>.+?) (?P<command>(?P=ident).+?))?"),
                "if <condition> <command>",
                "Excecute certain command when a message matching the condition is sent.",
                add,
                exec_condition=lambda match, message, bot: FLAG_NESTED not in message.flags
                ),

        # Show rules
        Command(RegexMatcher(r"!!rules?(?: ?(?:(?P<page>\d+)|(?P<condition>.+)))?"),
                "rules (<condition>|<page>)",
                "Display all rules by page (if page is given) or "
                "(if condition is given) display rules matching given condition.",
                show
                ),

        # Remove rule
        Command(RegexMatcher(r"!!removeif(?: (?P<condition>.+?))?"),
                "removeif <condition>",
                "Remove a rule by providing its condition literally.",
                remove,
                exec_condition=lambda match, message, bot: FLAG_NESTED not in message.flags
                ),

        # Remove rule by matching
        Command(RegexMatcher(r"!!removematch(?: (?P<condition>.+?))?"),
                "removematch <message>",
                "Remove rules that would match on given text.",
                remove_matching,
                exec_condition=lambda match, message, bot: FLAG_NESTED not in message.flags
                ),

        # Regexif
        Command(RegexMatcher(r"#regexif(?: (?P<bool>true|false))?"),
                "#regexif [true|false]",
                "Enable or disable the execution of rules.",
                set_regexif,
                display_condition=lambda match, message, bot: False,
                exec_condition=lambda matcg, message, bot: message.sent_by["is_mod"]
                ),
    )
